import requests
import argparse
import logging
import smtplib
import time

# Here are the email package modules we'll need
from email.mime.text import MIMEText

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def check_appts(appt_url):
    try:
        r = requests.get(appt_url, verify=False)
        return r.json()
    except:
        logger.info('Error: with request')
        return {}


# Out[9]: {u'empty': u'TRUE'}

# {"slots":[{"time":"12 December 2017 - 08:00", "id":"A75CED65B91E7C02802581B50031827D"},
#           {"time":"12 December 2017 - 17:00", "id":"EBA13E5074A9CF0E802581B500318292"},
#           {"time":"12 December 2017 - 18:00", "id":"53C5C8872B79E8E5802581B500318299"},
#           {"time":"12 December 2017 - 19:00", "id":"A42D9BC2C60B6067802581B5003182A9"}]}

# {u'slots': [{u'id': u'42E7EA2CBCA884C4802581B500318265',
#    u'time': u'12 December 2017 - 08:00'},
#   {u'id': u'67D997F4A64EDAED802581B500318297',
#    u'time': u'12 December 2017 - 18:00'},
#   {u'id': u'A42D9BC2C60B6067802581B5003182A9',
#    u'time': u'12 December 2017 - 19:00'}]}

# def parse_gnib_json(gnib_json):
def listen_for_gnib_appts(sender_email, recipients, check_frequency):

    # known_appt_types and appt_cache are globals
    appt_url = known_appt_types['gnib']
    # send startup message
    startup_msg_text = 'Hi,\n\nJust wanted to let you know that I started listening for GNIB appointments for you.\n' + \
                       'I\'ll be checking for new appointments every {} seconds, and I\'ll send you an email '.format(check_frequency) + \
                       'each time I find some new ones.\n\nCheers,\nGNIBBot'
    startup_msg_subj = 'I\'m watching for GNIB appointments'
    send_message(startup_msg_subj, startup_msg_text, sender_email, recipients)

    logger.info('Started listening to GNIB appt endpoint')
    while True:
        appts_obj = check_appts(appt_url)
        if 'slots' in appts_obj:
            new_appts = []
            for appt in appts_obj['slots']:
                if appt['id'] not in appt_cache:
                    new_appts.append(appt)
                    # add the appts to cache
                    appt_cache[appt['id']] = appt
            if len(new_appts) > 0:
                # send an email
                appt_template = u'Appointment Time: {}'
                appt_text = '\n'.join([appt_template.format(appt['time']) for appt in new_appts])
                appt_text = u'I found new appointments!\n\n' + appt_text
                appt_text = appt_text + u'\n\nGo to https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf to book'
                send_message(u'New Appointments Found', appt_text, sender_email, recipients)
                logger.info('Sending message: {}'.format(appt_text))

        # wait for some time
        time.sleep(5)


def listen_for_visa_appts(sender_email, recipients, check_frequency):
    # known_appt_types and appt_cache are globals
    appt_url = known_appt_types['visa']
    # send startup message
    startup_msg_text = 'Hi,\n\nJust wanted to let you know that I started listening for Visa appointments for you.\n' + \
                       'I\'ll be checking for new appointments every {} seconds, and I\'ll send you an email '.format(check_frequency) + \
                       'each time I find some new ones.\n\nCheers,\nGNIBBot'
    startup_msg_subj = 'I\'m watching for Visa appointments'
    send_message(startup_msg_subj, startup_msg_text, sender_email, recipients)

    logger.info('Started listening to Visa appt endpoint')
    while True:
        appts_obj = check_appts(appt_url)
        # {"dates": ["11/10/2017", "26/10/2017"]}
        if 'dates' in appts_obj and len(appts_obj['dates']) > 0:
            new_appts = []
            for date in appts_obj['dates']:
                if date not in appt_cache:
                    new_appts.append(date)
                    # add the appts to cache
                    appt_cache[date] = date
            if len(new_appts) > 0:
                # send an email
                appt_template = u'Appointment Date: {}'
                appt_text = '\n'.join([appt_template.format(date) for date in new_appts])
                appt_text = u'I found new dates for appointments!\n\n' + appt_text
                appt_text = appt_text + u'\n\nGo to https://reentryvisa.inis.gov.ie/website/INISOA/IOA.nsf/AppointmentSelection?OpenForm to book'
                send_message(u'New Visa Dates Found', appt_text, sender_email, recipients)
                logger.info('Sending message: {}'.format(appt_text))


        # wait for some time
        time.sleep(5)


# TODO: let user put their info in .yaml config, then actually book when appt is available
def run(appt_type, sender_email, recipients, check_frequency=5):

    if appt_type == 'visa':
        listen_for_visa_appts(sender_email, recipients, check_frequency)

    elif appt_type == 'gnib':
        listen_for_gnib_appts(sender_email, recipients, check_frequency)


COMMASPACE = ', '


def send_message(subj, text, sender_email, recipient_emails):
    # Create the container (outer) email message.
    # `text` is the message body
    msg = MIMEText(text)
    msg['Subject'] = subj
    # me == the sender's email address
    # emails = the list of all recipients' email addresses
    msg['From'] = sender_email
    msg['To'] = COMMASPACE.join(recipient_emails)
    msg.preamble = subj

    # Send the email via our own SMTP server.
    s = smtplib.SMTP('localhost')
    try:
        s.sendmail(sender_email, recipient_emails, msg.as_string())
    except:
        logger.error('Error sending message')
    s.quit()


if __name__ == '__main__':
    # args
    parser = argparse.ArgumentParser()
    parser.add_argument('--recipients', nargs='+', help='the addresses that will recieve notifications', required=True)
    parser.add_argument('--appt_type', required=True, type=str, help="one of {gnib|visa}")
    args = parser.parse_args()

    global appt_cache
    global known_appt_types

    appt_cache = {}
    # map appt types to URL
    known_appt_types = {
        'gnib': 'https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/(getAppsNear)?openpage&cat=Work&sbcat=All&typ=Renewal&_=1507580615406',
        'visa': 'https://reentryvisa.inis.gov.ie/website/INISOA/IOA.nsf/(getDTAvail)?openagent&type=I&_=1507712583422'
    }
    if args.appt_type not in known_appt_types:
        raise ValueError('Unknown appointment type: {}'.format(args.appt_type))

    # new_appt_cmd = ['echo', "new appointments", "festival", "--tts"]

    default_sender = 'chris@gni3bot.com'
    run(args.appt_type, default_sender, args.recipients, check_frequency=5)

